package cheeth.smet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.iflytek.cloud.FaceDetector;
import com.iflytek.cloud.SpeechUtility;

import cheeth.smet.camera.FaceTrackerActivity;
import cheeth.smet.danmu.DanMuActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FaceDetector faceDector = FaceDetector.createDetector(this, null);
    }

    public void onTorentou(View v){
        FaceTrackerActivity.startActivity(this);
    }

    public void onTodanmu(View v){
        DanMuActivity.startActivity(this);
    }

    public void onStopDanmu(){

    }
}
