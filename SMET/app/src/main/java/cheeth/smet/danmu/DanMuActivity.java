package cheeth.smet.danmu;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.iflytek.cloud.SpeechUtility;

import cheeth.smet.R;
import cheeth.smet.util.ApkInstaller;
import cheeth.smet.util.FucUtil;

/**
 * Created by sunxiaoming on 17-6-24.
 */

public class DanMuActivity extends AppCompatActivity {


    private static final int REQUEST_CODE = 77;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    ApkInstaller mInstaller;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > 23) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_CODE);
                return;
            }
        }

        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
            return;
        }

        mInstaller = new ApkInstaller(this);
        if (!SpeechUtility.getUtility().checkServiceInstalled()) {
            mInstaller.install();
            return;
        } else {
            String result = FucUtil.checkLocalResource();
            if (!TextUtils.isEmpty(result)) {
                Toast.makeText(this,result,Toast.LENGTH_LONG).show();
            }
        }
        DanmuService.startService(this,"");
        setContentView(R.layout.danmu_main);

//        DanmuService.startService(this,"dsflkjdslkfjdslk","http://img1.utuku.china.com/530x0/news/20170623/6f975e0a-ad30-4dc7-8ecc-ceca11b12cda.jpg");

    }

    public void onOpenDanmu(View v){
        DanmuService.startServiceOpenFloat(this);
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {

        final String[] permissions = new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }
    }

    public static boolean checkPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // we have permission, so create the camerasource
            return;
        }

    }

    private void runDanmu() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for(int i=0;i< 10;i++){
//                    DanmuService.startService(DanMuActivity.this,"sdklfjlkdjsflkjdslkfjdlskjfjdlskfjdsl;\t"+i);
//                    try {
//                        Thread.sleep(7000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();
    }

    public static void startActivity(Context context){
        Intent intent = new Intent();
        intent.setClass(context,DanMuActivity.class);
        context.startActivity(intent);
    }
}
