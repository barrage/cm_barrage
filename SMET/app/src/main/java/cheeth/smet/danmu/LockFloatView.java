package cheeth.smet.danmu;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import cheeth.smet.R;
import cheeth.smet.util.DimenUtils;


/**
 * Created by blue on 17-2-22.
 */


public class LockFloatView extends RelativeLayout {

    public static final int SIDE_MARGIN = 3;
    ValueAnimator mBackAnim;
    ValueAnimator mAlphaAnim;
    private static final float ALPHA_LIMIT = .6F;
    private GestureDetector mGestureDetector;

    public LockFloatView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.flaot_view,this);
        ViewCompat.setAlpha(LockFloatView.this,ALPHA_LIMIT);
        mGestureDetector = new GestureDetector(context, new LockFloatView.MyOnGestureListener());
    }

    public void setRecording(boolean result) {
        if(result){
            findViewById(R.id.icon).setBackground(getResources().getDrawable(R.drawable.circle_recording));
        }else{
            findViewById(R.id.icon).setBackground(getResources().getDrawable(R.drawable.circle));
        }
    }

    class MyOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            onClick();
            return false;
        }
    }

    public void onClick(){

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = handleEvent(event);
        return result || super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean result = handleEvent(ev);
        return result || super.onInterceptTouchEvent(ev);
    }


    private float mLastX,mLastY;
    private boolean handleEvent(MotionEvent ev){
        mGestureDetector.onTouchEvent(ev);
        if(null != mBackAnim && mBackAnim.isRunning()){
            return false;
        }
        float dx = ev.getRawX() - mLastX;
        float dy = ev.getRawY() - mLastY;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastX = ev.getRawX();
                mLastY = ev.getRawY();
                if(mAlphaAnim != null && mAlphaAnim.isRunning()){
                    mAlphaAnim.cancel();
                }
                ViewCompat.setAlpha(this,1f);
                break;
            case MotionEvent.ACTION_MOVE:
                if(canMove()){
                    updateView(dx,dy);
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if(needBackAnim()){
                    backAnim();
                }else{
                    if(needAlphaAnim()){
                        playAlphaAnim();
                    }
                }
                break;
            default:
                break;
        }
        mLastX = ev.getRawX();
        mLastY = ev.getRawY();
        return false;
    }

    private boolean needAlphaAnim() {
        return getAlpha() != ALPHA_LIMIT;
    }

    private void backAnim() {
        mBackAnim = ValueAnimator.ofInt(getCurrentX(),((getCurrentX() + getWidth()/2) > getScreenWidth()/2) ?
                getScreenWidth() - getWidth() - DimenUtils.dp2px(getContext(),SIDE_MARGIN):
                DimenUtils.dp2px(getContext(),SIDE_MARGIN));
        mBackAnim.setDuration(200);
        mBackAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int current = (int) animation.getAnimatedValue();
                updateViewX(current);
            }
        });
        mBackAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                playAlphaAnim();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mBackAnim.start();
    }

    private void playAlphaAnim() {
        mAlphaAnim = ValueAnimator.ofFloat(getAlpha(),ALPHA_LIMIT);
        mAlphaAnim.setDuration(500);
        mAlphaAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float current = (float) animation.getAnimatedValue();
                ViewCompat.setAlpha(LockFloatView.this,current);
            }
        });
        mAlphaAnim.start();
    }

    public void updateView(float dx,float dy){

    }

    public void updateViewX(float x){

    }

    public int getCurrentX(){
        return 0;
    }

    public int getScreenWidth(){
        return 0;
    }

    public boolean needBackAnim(){
        return false;
    }

    public boolean canMove(){
        return false;
    }

}
