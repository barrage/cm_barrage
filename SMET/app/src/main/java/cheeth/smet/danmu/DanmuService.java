package cheeth.smet.danmu;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Surface;
import android.view.WindowManager;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.FaceDetector;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.TreeMap;

import cheeth.smet.camera.APictureCapturingService;
import cheeth.smet.camera.PictureCapturingListener;
import cheeth.smet.camera.PictureCapturingServiceImpl;
import cheeth.smet.util.DimenUtils;
import cheeth.smet.util.FaceRect;
import cheeth.smet.util.FucUtil;
import cheeth.smet.util.JsonParser;
import cheeth.smet.util.ParseResult;

/**
 * Created by sunxiaoming on 17-6-24.
 */

public class DanmuService extends Service implements PictureCapturingListener {

    public static final String ACTION_SHOW_TEXT = "ACTION_SHOW_TEXT";
    public static final String ACTION_SHOW_FLOAT = "ACTION_SHOW_FLOAT";
    public static final String ACTION_SHOW_TEXT_FOR_OTHER = "ACTION_SHOW_TEXT_FOR_OTHER";
    public static final int TOP_BOTTOM_MARGIN = 2;
    private static WindowManager mWm = null;
    private int mScreenWidth;
    private int mScrrenHeight;
    private ArrayList<DanmuDao> mDaoList = new ArrayList<>();
    private static final String EXTRA_TEXT = ":extra_text";
    private static final String EXTRA_IMAGE_URL = ":extra_image_url";
    private APictureCapturingService pictureService;
    private int mRotation;
    private Paint mPaint;

    private LockFloatView mFloatView;

    private FaceDetector mFaceDetector;
    private FaceRect[] mFaces;
    private int mTopBottomMargin = 0;

    // 语音听写对象
    private SpeechRecognizer mIat;
    int ret = 0; // 函数调用返回值

    private WindowManager.LayoutParams mLockFloatParams = new WindowManager.LayoutParams();

    private HashMap<String, String> mIatResults = new LinkedHashMap<String, String>();
    private LinkedList<DanmuDao> mLinkedDanmu = new LinkedList<>();

    private static int mEmptyViewFlags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mScreenWidth = getScreenWidth(getApplicationContext());
        mScrrenHeight = DimenUtils.getScreenHeight(getApplicationContext());
        pictureService = PictureCapturingServiceImpl.getInstance(this);
        mRotation = getRotation(getApplicationContext());
        mFaceDetector = FaceDetector.createDetector(this, null);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(DimenUtils.dp2px(getApplicationContext(),1));
        mPaint.setColor(0xffffff00);
        mTopBottomMargin = DimenUtils.dp2px(getApplicationContext(), TOP_BOTTOM_MARGIN);
        mIat = SpeechRecognizer.createRecognizer(getApplicationContext(), mInitListener);
        initLeftTopFloatViewParams();
    }

    /**
     * 初始化监听器。
     */
    private InitListener mInitListener = new InitListener() {

        @Override
        public void onInit(int code) {
            Log.d("yao", "SpeechRecognizer init() code = " + code);
            if (code != ErrorCode.SUCCESS) {
                Log.d("yao","初始化失败，错误码：" + code);
            }
        }
    };


    private WindowManager.LayoutParams initLeftTopFloatViewParams() {
        mLockFloatParams.type = Build.VERSION.SDK_INT > 23 ? WindowManager.LayoutParams.TYPE_PHONE : WindowManager.LayoutParams.TYPE_TOAST;
        mLockFloatParams.format = PixelFormat.RGBA_8888;
        mLockFloatParams.flags = mEmptyViewFlags;
        mLockFloatParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
        mLockFloatParams.y = mScrrenHeight - mTopBottomMargin - DimenUtils.dp2px(getApplicationContext(), 40) - mScrrenHeight/6;
        mLockFloatParams.x = mScreenWidth - DimenUtils.dp2px(getApplicationContext(), 37 + LockFloatView.SIDE_MARGIN);
        mLockFloatParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mLockFloatParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        return mLockFloatParams;
    }

    private void   showFloatView() {
        mFloatView = new LockFloatView(getApplicationContext()) {
            @Override
            public void updateView(float x, float y) {
                updateFloatView(x, y);
            }

            @Override
            public void updateViewX(float x) {
                updateFloatViewX(x);
            }

            @Override
            public int getScreenWidth() {
                return mScreenWidth;
            }

            @Override
            public int getCurrentX() {
                return mLockFloatParams.x;
            }

            @Override
            public boolean needBackAnim() {
                return mFloatView != null && !(mLockFloatParams.x == 0 || mLockFloatParams.x == mScreenWidth - mFloatView.getWidth());
            }

            @Override
            public boolean canMove() {
                return true;
            }

            @Override
            public void onClick() {
                if(!mIat.isListening()){
                    mLinkedDanmu.clear();
                    mFloatView.setRecording(true);
                    setParam();
                    ret = mIat.startListening(mRecognizerListener);
                }else{
                    mIat.stopListening();
                }
            }
        };
        try {
            mWm.addView(mFloatView, mLockFloatParams);
        } catch (Exception e) {
            mFloatView = null;
            e.printStackTrace();
        }
    }

    /**
     * 听写监听器。
     */
    private RecognizerListener mRecognizerListener = new RecognizerListener() {

        @Override
        public void onBeginOfSpeech() {
            mIatResults.clear();
            // 此回调表示：sdk内部录音机已经准备好了，用户可以开始语音输入
            log("开始说话");
        }

        @Override
        public void onError(SpeechError error) {
            // Tips：
            // 错误码：10118(您没有说话)，可能是录音机权限被禁，需要提示用户打开应用的录音权限。
            // 如果使用本地功能（语记）需要提示用户开启语记的录音权限。
            log(error.getPlainDescription(true));
        }

        @Override
        public void onEndOfSpeech() {
            // 此回调表示：检测到了语音的尾端点，已经进入识别过程，不再接受语音输入
            log("结束说话");
            mFloatView.setRecording(false);
        }

        @Override
        public void onResult(RecognizerResult results, boolean isLast) {
            log(results.getResultString());
            printResult(results,isLast);
        }

        @Override
        public void onVolumeChanged(int volume, byte[] data) {
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            // 若使用本地能力，会话id为null
            //	if (SpeechEvent.EVENT_SESSION_ID == eventType) {
            //		String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
            //		Log.d(TAG, "session id =" + sid);
            //	}
        }
    };

    private void printResult(RecognizerResult results, boolean isLast) {
        String text = JsonParser.parseIatResult(results.getResultString());

        String sn = null;
        // 读取json结果中的sn字段
        try {
            JSONObject resultJson = new JSONObject(results.getResultString());
            sn = resultJson.optString("sn");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mIatResults.put(sn, text);

        StringBuffer resultBuffer = new StringBuffer();
        for (String key : mIatResults.keySet()) {
            resultBuffer.append(mIatResults.get(key));
        }

        if(isLast){
            mFloatView.setRecording(false);
            log("44444444444444444444444444444444444\t"+resultBuffer.toString());
            startService(getApplicationContext(),resultBuffer.toString());
        }

    }

    /**
     * 参数设置
     *
     * @return
     */
    public void setParam() {
        // 清空参数
        mIat.setParameter(SpeechConstant.PARAMS, null);

        // 设置听写引擎
        mIat.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
        // 设置返回结果格式
        mIat.setParameter(SpeechConstant.RESULT_TYPE, "json");

        String lag = "mandarin";
        if (lag.equals("en_us")) {
            // 设置语言
            mIat.setParameter(SpeechConstant.LANGUAGE, "en_us");
            mIat.setParameter(SpeechConstant.ACCENT, null);
        } else {
            // 设置语言
            mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
            // 设置语言区域
            mIat.setParameter(SpeechConstant.ACCENT, lag);
        }

        // 设置语音前端点:静音超时时间，即用户多长时间不说话则当做超时处理
        mIat.setParameter(SpeechConstant.VAD_BOS, "4000");

        // 设置语音后端点:后端点静音检测时间，即用户停止说话多长时间内即认为不再输入， 自动停止录音
        mIat.setParameter(SpeechConstant.VAD_EOS, "1000");

        // 设置标点符号,设置为"0"返回结果无标点,设置为"1"返回结果有标点
        mIat.setParameter(SpeechConstant.ASR_PTT, "1");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mIat.setParameter(SpeechConstant.AUDIO_FORMAT,"wav");
        mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/iat.wav");
    }

    private void updateFloatViewX(float x) {
        mLockFloatParams.x = (int) x;
        updateView();
    }

    private void updateFloatView(float dx, float dy) {
        mLockFloatParams.x += dx;
        mLockFloatParams.y -= dy;
        updateView();
    }

    private void updateView() {
        if (mFloatView == null) {
            return;
        }
        if (mLockFloatParams.x < DimenUtils.dp2px(getApplicationContext(),LockFloatView.SIDE_MARGIN)) {
            mLockFloatParams.x = DimenUtils.dp2px(getApplicationContext(),LockFloatView.SIDE_MARGIN);
        }

        if (mLockFloatParams.x > (mScreenWidth - mFloatView.getWidth() - DimenUtils.dp2px(getApplicationContext(),LockFloatView.SIDE_MARGIN))) {
            mLockFloatParams.x = mScreenWidth - mFloatView.getWidth()- DimenUtils.dp2px(getApplicationContext(),LockFloatView.SIDE_MARGIN);
        }

        if (mLockFloatParams.y < mTopBottomMargin) {
            mLockFloatParams.y = mTopBottomMargin;
        }

        if (mLockFloatParams.y > mScrrenHeight - mFloatView.getHeight() ) {
            mLockFloatParams.y = mScrrenHeight - mFloatView.getHeight();
        }
        try {
            mWm.updateViewLayout(mFloatView, mLockFloatParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent,int flags, int startId) {
        if(null != intent){
            String action = intent.getAction();
            if(ACTION_SHOW_TEXT.equals(action)){
                String text = intent.getStringExtra(EXTRA_TEXT);
                if(!TextUtils.isEmpty(text)){
                    log("onStartCommand\t"+text);
                    DanmuDao dao = new DanmuDao(getApplicationContext(),mWm,mScreenWidth);
                    dao.setText(text);
                    mLinkedDanmu.add(dao);
                    pictureService.startCapturing(this);
                }
            }else if(ACTION_SHOW_TEXT_FOR_OTHER.equals(action)){
                String text = intent.getStringExtra(EXTRA_TEXT);
                String imageUrl = intent.getStringExtra(EXTRA_IMAGE_URL);
                if(!TextUtils.isEmpty(text)){
//                    log("onStartCommand\t"+text);
                    DanmuDao dao = new DanmuDao(getApplicationContext(),mWm,mScreenWidth);
                    dao.setText(text);
                    dao.setImageUrl(imageUrl);
                    new MyDownloadTask(dao).execute(dao);
                }
            }else if(ACTION_SHOW_FLOAT.equals(action)){
                showFloatView();
            }
        }
        return START_STICKY;
    }

    private class MyDownloadTask extends AsyncTask<DanmuDao, Void, Bitmap> {

        private DanmuDao mMyDao;

        public MyDownloadTask(DanmuDao dao){
            mMyDao = dao;
        }

        @Override
        protected Bitmap doInBackground(DanmuDao... params) {
            DanmuDao dao = params[0];
            try {
                byte[] imageByte =  FucUtil.getImage(dao.getImageUrl());
                return BitmapFactory.decodeByteArray(imageByte,0,imageByte.length);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            mDaoList.add(mMyDao);
            mMyDao.addFloatView(getSpareOffset(),bitmap);
        }
    }

    private int getSpareOffset() {
        int offset = 0;
        for(DanmuDao dao : mDaoList){
            if(dao.isIsAlive()){
                if(dao.getCurrenY() + DimenUtils.dp2px(getApplicationContext(),70) < mScrrenHeight){
                    offset += DimenUtils.dp2px(getApplicationContext(),60);
                }else{
                    offset = 0;
                }
            }
        }
        return offset;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mScreenWidth = getScreenWidth(getApplicationContext());
        mScrrenHeight = DimenUtils.getScreenHeight(getApplicationContext());
        mRotation = getRotation(getApplicationContext());
        log("!!!!!!!!!!!!!!!!!!!!!!!!\t"+mRotation);
    }

    public int getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 270;
            case Surface.ROTATION_90:
                return 0;
            case Surface.ROTATION_180:
                return 0;
            default:
                return 180;
        }
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.widthPixels;
    }



    private void log(String text){
        Log.i("yao",text);
    }

    public static void startService(Context context,String text){
        Intent intent = new Intent();
        intent.setAction(ACTION_SHOW_TEXT);
        intent.putExtra(EXTRA_TEXT,text);
        intent.setClass(context,DanmuService.class);
        context.startService(intent);
    }

    public static void startServiceOpenFloat(Context context){
        Intent intent = new Intent();
        intent.setAction(ACTION_SHOW_FLOAT);
        intent.setClass(context,DanmuService.class);
        context.startService(intent);
    }

    public static void startService(Context context,String text,String imageUrl){
        Intent intent = new Intent();
        intent.setAction(ACTION_SHOW_TEXT_FOR_OTHER);
        intent.putExtra(EXTRA_TEXT,text);
        intent.putExtra(EXTRA_IMAGE_URL,imageUrl);
        intent.setClass(context,DanmuService.class);
        context.startService(intent);
    }

    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        new MyTask().execute(pictureData);
    }

    private class MyTask extends AsyncTask<byte[], Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(byte[]... params) {
            byte[] pictureData = params[0];
            final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
            final int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            final Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

            String result = mFaceDetector.detectARGB(scaled);
            Log.d("yao", "result:"+result);
            // 解析人脸结果
            mFaces = ParseResult.parseResult(result);
            Bitmap faceResult = drawFaces(scaled,mFaces);
            return faceResult;
        }

        @Override
        protected void onPostExecute(final Bitmap bitmap) {
            super.onPostExecute(bitmap);
            log("onCaptureDone");
            final DanmuDao dao = mLinkedDanmu.poll();
            if(null == dao){
                return;
            }
            mDaoList.add(dao);
            dao.addFloatView(getSpareOffset(),bitmap);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String result = FucUtil.postData(getApplicationContext(),dao.getText(),bitmap);
                    log("post result\t"+result);
                }
            }).start();
        }
    }

        private Bitmap drawFaces(Bitmap scaled, FaceRect[] mFaces) {
        if(null != mFaces && mFaces.length > 0){
            FaceRect face = mFaces[0];
            int offset = face.bound.width()/3;
            face.bound.left -= offset;
            face.bound.right += offset;
            face.bound.top -= offset;
            face.bound.bottom += offset;
            Bitmap reulst = Bitmap.createBitmap(face.bound.width(), face.bound.height(), scaled.getConfig());
            Canvas canvas = new Canvas(reulst);
            canvas.drawBitmap(scaled,-face.bound.left,-face.bound.top,null);
            return reulst;

        }
        return scaled;
    }

    @Override
    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {

    }

    @Override
    public int getRotation() {
        return mRotation;
    }




}
