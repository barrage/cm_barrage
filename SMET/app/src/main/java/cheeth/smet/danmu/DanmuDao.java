package cheeth.smet.danmu;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import cheeth.smet.R;
import cheeth.smet.util.DimenUtils;

/**
 * Created by sunxiaoming on 17-6-24.
 */

public class DanmuDao {
    private int mCurrenY;
    private View mFloatView;
    private WindowManager.LayoutParams mLockFloatParams = new WindowManager.LayoutParams();
    private int mEmptyViewFlags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;

    private static WindowManager mWm = null;
    private boolean mIsAlive = false;
    private Context mContext;
    private String mText;
    private String mImageUrl;
    private int mScreenWidth;

    private int[] mColor = new int[]{0xFFFFFFFF,0xFFFF0000,0xFF00FF00,0xFF0000FF};
    private Random mRandom = new Random();

    public DanmuDao(Context context, WindowManager wm, int screenWidth) {
        mContext = context;
        mWm = wm;
        mScreenWidth = screenWidth;
    }

    public void setText(String text){
        mText = text;
    }

    public String getText(){
        return mText;
    }

    public void addFloatView(int yOffset,Bitmap map) {
        mFloatView = LayoutInflater.from(mContext).inflate(R.layout.float_view, null);
        if(null != map){
            Log.i("yao","map\t"+map);
            ((ImageView) mFloatView.findViewById(R.id.image)).setImageBitmap(map);
        }
        ((TextView) mFloatView.findViewById(R.id.text)).setText(mText);
        ((TextView) mFloatView.findViewById(R.id.text)).setTextColor(mColor[mRandom.nextInt(4)]);
        mLockFloatParams.width = (int) (((TextView) mFloatView.findViewById(R.id.text)).getPaint().measureText(mText))+ DimenUtils.dp2px(mContext,80);
        mLockFloatParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mLockFloatParams.flags = mEmptyViewFlags;
        mLockFloatParams.x = 0;
        mLockFloatParams.format = PixelFormat.TRANSPARENT;
        mLockFloatParams.gravity = Gravity.TOP | Gravity.LEFT;
        mLockFloatParams.y = 500 + yOffset;
        mLockFloatParams.type = Build.VERSION.SDK_INT > 23 ? WindowManager.LayoutParams.TYPE_PHONE : WindowManager.LayoutParams.TYPE_TOAST;
        mWm.addView(mFloatView, mLockFloatParams);
        startAnim(mLockFloatParams.width);
        mCurrenY = mLockFloatParams.y;
        mIsAlive = true;
    }

    public boolean isIsAlive(){
        return mIsAlive;
    }

    public int getCurrenY(){
        return mCurrenY;
    }

    private void startAnim(int width) {
        ValueAnimator animator = ValueAnimator.ofInt(mScreenWidth, - width);
        animator.setDuration(3000);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (int) animation.getAnimatedValue();
                updateFloatView(value);
            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hideFloatView();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    private void updateFloatView(int value) {
        if (null != mFloatView) {
            mLockFloatParams.x = value;
            mWm.updateViewLayout(mFloatView, mLockFloatParams);
        }
    }

    private void hideFloatView() {
        try {
            if (null != mFloatView && mFloatView.getParent() != null) {
                mWm.removeView(mFloatView);
            }
            mFloatView = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl(){
        return mImageUrl;
    }
}
