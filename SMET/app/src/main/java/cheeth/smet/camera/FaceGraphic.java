/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cheeth.smet.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.google.android.gms.vision.face.Face;

import cheeth.smet.R;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;

    private static final int COLOR_CHOICES[] = {
            Color.BLUE,
            Color.CYAN,
            Color.GREEN,
            Color.MAGENTA,
            Color.RED,
            Color.WHITE,
            Color.YELLOW
    };
    private static int mCurrentColorIndex = 0;

    private Paint mFacePositionPaint;
    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;
    private int mFaceId;
    private float mFaceHappiness;
    private static Bitmap[] mTouxiangs = new Bitmap[7];

    public static final int TOU_FUSHENG = 0;
    public static final int TOU_XUMING = 1;
    public static final int TOU_ZHANGQUANLING = 2;
    public static final int TOU_GAOXIAOSONG = 3;
    public static final int TOU_LUHAN = 4;
    public static final int TOU_REBA = 5;
    public static final int TOU_CHAERSI = 6;


    private RectF mTouRect = new RectF();

    FaceGraphic(final GraphicOverlay overlay) {
        super(overlay);

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);

        mIdPaint = new Paint();
        mIdPaint.setColor(selectedColor);
        mIdPaint.setTextSize(ID_TEXT_SIZE);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
        if(mTouxiangs[0] == null){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.i("yao","FaceGraphic");
                    mTouxiangs[TOU_FUSHENG] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.fusheng);
                    mTouxiangs[TOU_XUMING] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.xuming);
                    mTouxiangs[TOU_ZHANGQUANLING] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.zhangquanling);
                    mTouxiangs[TOU_GAOXIAOSONG] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.gaoxiaosong);
                    mTouxiangs[TOU_LUHAN] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.luhan);
                    mTouxiangs[TOU_REBA] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.reba);
                    mTouxiangs[TOU_CHAERSI] = BitmapFactory.decodeResource(overlay.getContext().getResources(), R.drawable.chaersi);
                }
            }).start();
        }

    }

    void setId(int id) {
        mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;
        postInvalidate();
    }

    public int getBoundLeft(){
        Face face = mFace;
        if (face == null) {
            return -1;
        }

        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float xOffset = scaleX(face.getWidth() / 2.2f);
        float left = x - xOffset;
        return (int)left;
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;
        if (face == null) {
            return;
        }

        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);
        canvas.drawCircle(x, y, FACE_POSITION_RADIUS, mFacePositionPaint);

        // Draws a bounding box around the face.
        float xOffset = scaleX(face.getWidth() / 2.2f);
        float yOffset = scaleY(face.getHeight() / 2.1f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        mTouRect.set(left,top,right,bottom);
        canvas.drawRect(left, top, right, bottom, mBoxPaint);
        drawTouXiang(mTouRect,canvas);

    }

    private void drawTouXiang(RectF mTouRect, Canvas canvas) {
        for(TouXiangDao dao : FaceTrackerActivity.sTouXiang){
            if(dao.mId == mFaceId && null != mTouxiangs[dao.mType]){
                canvas.drawBitmap(mTouxiangs[dao.mType],null,mTouRect,null);
            }
        }
    }

    public int getId() {
        return mFaceId;
    }
}
