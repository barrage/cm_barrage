package cheeth.smet;

import org.json.JSONObject;

/**
 * Created by sunxiaoming on 2017/6/25.
 */

public class SendDanMuItem {
    public String type;
    public String aid;
    public String txt;
    public String img;
    public String font;
    public String fontsize;

    public SendDanMuItem(JSONObject object) {
        try {
            this.type = object.getString("type");
            this.aid = object.getString("aid");
            this.txt = object.getString("txt");
            this.img = object.getString("img");
            this.font = object.getString("font");
            this.fontsize = object.getString("fontsize");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
