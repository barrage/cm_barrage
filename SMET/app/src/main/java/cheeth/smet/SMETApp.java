package cheeth.smet;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

public class SMETApp extends Application {


    private String mProcName = "";

    @Override
    public void onCreate() {
        // 应用程序入口处调用，避免手机内存过小，杀死后台进程后通过历史intent进入Activity造成SpeechUtility对象为null
        // 如在Application中调用初始化，需要在Mainifest中注册该Applicaiton
        // 注意：此接口在非主进程调用会返回null对象，如需在非主进程使用语音功能，请增加参数：SpeechConstant.FORCE_LOGIN+"=true"
        // 参数间使用半角“,”分隔。
        // 设置你申请的应用appid,请勿在'='与appid之间添加空格及空转义符

        // 注意： appid 必须和下载的SDK保持一致，否则会出现10407错误

        SpeechUtility.createUtility(SMETApp.this, "appid=" + "594e2ebc"+","+ SpeechConstant.FORCE_LOGIN+"=true");
        Log.i("yao", "onCreate-------------");

        // 以下语句用于设置日志开关（默认开启），设置成false时关闭语音云SDK日志打印
        // Setting.setShowLog(false);
        Log.i("yao", "start 1");
        PushAgent pushAgent = PushAgent.getInstance(this);
        pushAgent.setDebugMode(true);
        Log.i("yao", "start 2");
        pushAgent.register(new IUmengRegisterCallback() {
            @Override
            public void onSuccess(String deviceToken) {
                Log.i("yao", "device token: " + deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.i("yao", "register failed: " + s + " " + s1);
            }
        });
        Log.i("yao", "start 3");
        pushAgent.setPushIntentServiceClass(UmengNotificationService.class);
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        mProcName = getProcessName(base);
        RuntimeCheck.Init(mProcName);
    }

    public String getProcessName(Context base) {
        File cmdFile = new File("/proc/self/cmdline");

        if (cmdFile.exists() && !cmdFile.isDirectory()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(cmdFile)));
                String procName = reader.readLine();

                if (!TextUtils.isEmpty(procName))
                    return procName.trim();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //try to fix SELinux limit due to unable access /proc file system
            ActivityManager am = (ActivityManager) base.getSystemService(Context.ACTIVITY_SERVICE);
            if (null != am) {
                List<ActivityManager.RunningAppProcessInfo> appProcessInfoList = am.getRunningAppProcesses();
                if (null != appProcessInfoList) {
                    for (ActivityManager.RunningAppProcessInfo i : appProcessInfoList) {
                        if (i.pid == android.os.Process.myPid()) {
                            return i.processName.trim();
                        }
                    }
                }
            }
        }

        //Warnning: getApplicationInfo().processName only return package name for some reason, you will not see
        // the real process name, such as com.cleanmaster.mguard:service
        return SMETApp.this.getApplicationInfo().processName;
    }
}
