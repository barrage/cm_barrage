package cheeth.smet.util;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * 功能性函数扩展类
 */
public class FucUtil {
	/**
	 * 读取asset目录下文件。
	 * @return content
	 */
	public static String readFile(Context mContext, String file, String code)
	{
		int len = 0;
		byte []buf = null;
		String result = "";
		try {
			InputStream in = mContext.getAssets().open(file);
			len  = in.available();
			buf = new byte[len];
			in.read(buf, 0, len);
			
			result = new String(buf,code);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * 将字节缓冲区按照固定大小进行分割成数组
	 * @param buffer 缓冲区
	 * @param length 缓冲区大小
	 * @param spsize 切割块大小
	 * @return
	 */
	public ArrayList<byte[]> splitBuffer(byte[] buffer, int length, int spsize)
	{
		ArrayList<byte[]> array = new ArrayList<byte[]>();
		if(spsize <= 0 || length <= 0 || buffer == null || buffer.length < length)
			return array;
		int size = 0;
		while(size < length)
		{
			int left = length - size;
			if(spsize < left)
			{
				byte[] sdata = new byte[spsize];
				System.arraycopy(buffer,size,sdata,0,spsize);
				array.add(sdata);
				size += spsize;
			}else
			{
				byte[] sdata = new byte[left];
				System.arraycopy(buffer,size,sdata,0,left);
				array.add(sdata);
				size += left;
			}
		}
		return array;
	}
	/**
	 * 获取语记是否包含离线听写资源，如未包含跳转至资源下载页面
	 *1.PLUS_LOCAL_ALL: 本地所有资源 
      2.PLUS_LOCAL_ASR: 本地识别资源
      3.PLUS_LOCAL_TTS: 本地合成资源
	 */
	public static String checkLocalResource(){
		String resource = SpeechUtility.getUtility().getParameter(SpeechConstant.PLUS_LOCAL_ASR);
		try {
			JSONObject result = new JSONObject(resource);
			int ret = result.getInt(SpeechUtility.TAG_RESOURCE_RET);
			switch (ret) {
			case ErrorCode.SUCCESS:
				JSONArray asrArray = result.getJSONObject("result").optJSONArray("asr");
				if (asrArray != null) {
					int i = 0;
					// 查询否包含离线听写资源
					for (; i < asrArray.length(); i++) {
						if("iat".equals(asrArray.getJSONObject(i).get(SpeechConstant.DOMAIN))){
							//asrArray中包含语言、方言字段，后续会增加支持方言的本地听写。
							//如："accent": "mandarin","language": "zh_cn"
							break;
						}
					}
					if (i >= asrArray.length()) {
						
						SpeechUtility.getUtility().openEngineSettings(SpeechConstant.ENG_ASR);
						return "没有听写资源，跳转至资源下载页面";
					}
				}else {
					SpeechUtility.getUtility().openEngineSettings(SpeechConstant.ENG_ASR);
					return "没有听写资源，跳转至资源下载页面";
				}
				break;
			case ErrorCode.ERROR_VERSION_LOWER:
				return "语记版本过低，请更新后使用本地功能";
			case ErrorCode.ERROR_INVALID_RESULT:
				SpeechUtility.getUtility().openEngineSettings(SpeechConstant.ENG_ASR);
				return "获取结果出错，跳转至资源下载页面";
			case ErrorCode.ERROR_SYSTEM_PREINSTALL:
				//语记为厂商预置版本。
			default:
				break;
			}
		} catch (Exception e) {
			SpeechUtility.getUtility().openEngineSettings(SpeechConstant.ENG_ASR);
			return "获取结果出错，跳转至资源下载页面";
		}
		return "";
	}
	
	/**
	 * 读取asset目录下音频文件。
	 * 
	 * @return 二进制文件数据
	 */
	public static byte[] readAudioFile(Context context, String filename) {
		try {
			InputStream ins = context.getAssets().open(filename);
			byte[] data = new byte[ins.available()];
			
			ins.read(data);
			ins.close();
			
			return data;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public static String getMD5(String val) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(val.getBytes());
		byte[] m = md5.digest();//加密
		return getString(m);
	}

	private static String getString(byte[] b){
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < b.length; i ++){
			sb.append(b[i]);
		}
		return sb.toString();
	}

	public static  String postData(Context context,String text,Bitmap bmpScale) {
		byte[] data = null;
		try {
			String url = "https://hack.anychat.im/index/barrage";
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			MultipartEntity entity = new MultipartEntity();

			if(bmpScale!=null){
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				bmpScale.compress(Bitmap.CompressFormat.JPEG, 100, bos);
				data = bos.toByteArray();
				entity.addPart("file", new ByteArrayBody(data,"image/jpeg", FucUtil.getMD5("test2.jpg"+data.hashCode())));
			}
			entity.addPart("txt", new StringBody(text,"text/plain", Charset.forName("UTF-8")));
			entity.addPart("font", new StringBody("small","text/plain", Charset.forName("UTF-8")));
			entity.addPart("fontsize", new StringBody("18","text/plain", Charset.forName("UTF-8")));
			entity.addPart("aid", new StringBody(getAndroidID(context),"text/plain", Charset.forName("UTF-8")));

			httppost.setEntity(entity);
			HttpResponse resp = httpclient.execute(httppost);
			HttpEntity resEntity = resp.getEntity();
			String string= EntityUtils.toString(resEntity);
			return string;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return "";
	}

	static public String getAndroidID(Context context) {
		try {
			ContentResolver cr = context.getContentResolver();
			return  android.provider.Settings.System.getString(cr, android.provider.Settings.System.ANDROID_ID);
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * Get image from newwork
	 * @param path The path of image
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] getImage(String path) throws Exception{
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(5 * 1000);
		conn.setRequestMethod("GET");
		InputStream inStream = conn.getInputStream();
		if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
			return readStream(inStream);
		}
		return null;
	}

	/**
	 * Get image from newwork
	 * @param path The path of image
	 * @return InputStream
	 * @throws Exception
	 */
	public static InputStream getImageStream(String path) throws Exception{
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(5 * 1000);
		conn.setRequestMethod("GET");
		if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
			return conn.getInputStream();
		}
		return null;
	}
	/**
	 * Get data from stream
	 * @param inStream
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] readStream(InputStream inStream) throws Exception{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while( (len=inStream.read(buffer)) != -1){
			outStream.write(buffer, 0, len);
		}
		outStream.close();
		inStream.close();
		return outStream.toByteArray();
	}

}
