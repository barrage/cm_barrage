package cheeth.smet;

public class RuntimeCheck {
	public static String CLEANMASTER_CRASH_FEEDBACK_PROCESSNAME = ":crash.feedback";
	public static String CLEANMASTER_SERVICE_PROCESSNAME = ":channel";

    private static Thread  s_mainThread			    = null;
	private static boolean s_bIsSerivceProcess      = false;
	private static boolean s_bIsUiProcess		    = false;
	private static boolean s_bIsCrashProcess        = false;

	public static void Init(String procName){
		s_mainThread = Thread.currentThread();
		if (procName.contains(CLEANMASTER_SERVICE_PROCESSNAME) ){
			s_bIsSerivceProcess = true;
		}else if (procName.contains(CLEANMASTER_CRASH_FEEDBACK_PROCESSNAME) ){
			s_bIsCrashProcess = true;
		}else {
			s_bIsUiProcess = true;
		}
		
	}
	
	public static void SetServiceProcess(){
		s_bIsUiProcess = false;
		s_bIsSerivceProcess = true;
		s_bIsCrashProcess = false;
	}
	
	public static void CheckUiProcess(){
		if ( !s_bIsUiProcess ){
			throw new RuntimeException("Must run in UI Process");
		}
	}

	public static void CheckServiceProcess(){
		if ( !s_bIsSerivceProcess ){
			throw new RuntimeException("Must run in Service Process");
		}
	}
	
	public static void CheckMainUIThread(){
		if ( Thread.currentThread() != s_mainThread ){
			throw new RuntimeException("Must run in UI Thread");
		}
	}
	
	public static void CheckNoUIThread(){
		if ( Thread.currentThread() == s_mainThread ){
			throw new RuntimeException("Must not run in UI Thread");
		}
	}

	public static boolean IsCrashProcess(){
		return s_bIsCrashProcess;
	}
	
	public static boolean IsUIProcess(){
		return s_bIsUiProcess;
	}
	
	public static boolean IsServiceProcess(){
		return s_bIsSerivceProcess;
	}
}
